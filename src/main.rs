use std::fs;
use std::time::Instant;
use std::sync::Arc;
use std::thread;
use std::sync::mpsc::{channel,Sender,Receiver};

use image::{RgbaImage,Rgba,GenericImageView};

type Pixel = [u8; 4];

const SAMPLE_SIZE: (usize, usize) = (64, 64);
const LINE_SPACING: usize = 8;
const MAX_DIFF: u64 = (SAMPLE_SIZE.0 * SAMPLE_SIZE.1 / LINE_SPACING * 30) as u64;

const BASE_SIZE: usize = 250;
const START_FRESH: bool = true;

type Sample = [i16; SAMPLE_SIZE.0 * SAMPLE_SIZE.1 / LINE_SPACING];

fn main() {
    let mut images = Vec::new();
    let mut samples = Vec::new();

    let name = if START_FRESH { "start.jpg" } else { "output.png" };
    let map_img = image::open(name).unwrap().to_rgba8();
    let mut map_size = (map_img.width() as usize, map_img.height() as usize);
    let mut map = vec![[0; 4]; map_size.0 * map_size.1];
    let rad = map_size.0 as f64 * map_size.1 as f64 * 0.25;
    for x in 0..map_size.0 {
        for y in 0..map_size.1 {
            if START_FRESH && (x as f64 - map_size.0 as f64 / 2.0).powi(2) + (y as f64 - map_size.1 as f64 / 2.0).powi(2) >= rad {
                continue; //ignore the circular black edges
            }
            map[x + y * map_size.0] = map_img.get_pixel(x as u32, y as u32).0;
        }
    }
    let mut map = Arc::new(map);

    let entries = fs::read_dir("input/").unwrap();
    for entry in entries.filter_map(|entry| entry.ok()) {
        let path = entry.path();
        if !path.is_file() { continue; }
        match load_image(&path, &mut images, &mut samples) {
            Ok(()) => {},
            Err(e) => eprintln!("failed to load image {:?}: {}",path,e),
        }
    }

    let mut threads = vec![];

    let (send, incoming) = channel();
    for offset in 0..LINE_SPACING {
        let (outgoing, recv) = channel();
        threads.push(outgoing);
        let state = ThreadState {
            offset,
            samples: samples.clone(),
            map: vec![],
            map_size: (0, 0),
        };
        state.dispatch(map.clone(), map_size, recv, send.clone());
    }

    let mut last_update = 0;

    let mut sample_num = samples.len() - 1;
    loop {
        for outgoing in &threads {
            outgoing.send(CalcDiff(sample_num)).unwrap();
        }
        let mut best = u64::max_value();
        let mut best_pos = (0, 0);
        for _ in 0..threads.len() {
            let (i, diff, pos) = incoming.recv().unwrap();
            assert!(i == sample_num);
            if diff < best {
                best = diff;
                best_pos = pos;
            }
        }
        if best < MAX_DIFF {
            println!("placing image {} at {:?}",sample_num,best_pos);
            let (image, size) = images.remove(sample_num);
            samples.remove(sample_num);
            //for now, image indices are same as sample indices, but later we'll have multiple samples per image
            map = Arc::new(update_map(best_pos, &mut map_size, &map, &image, size));
            for outgoing in &threads {
                outgoing.send(MapUpdate{ map: map.clone(), map_size, samples: vec![sample_num], }).unwrap();
            }
            save_output(map_size, &map);
            last_update = 0;
        } else if last_update > samples.len() {
            break;
        }
        last_update += 1;
        if sample_num == 0 {
            sample_num = samples.len();
        }
        sample_num -= 1;
    }

    save_output(map_size, &map);
}

#[derive(Debug,Clone)]
enum ThreadMsg {
    CalcDiff(usize),
    MapUpdate{ map: Arc<Vec<Pixel>>, map_size: (usize, usize), samples: Vec<usize>, },
}
use ThreadMsg::*;

struct ThreadState {
    offset: usize,
    samples: Vec<[i16; SAMPLE_SIZE.0 * SAMPLE_SIZE.1 / LINE_SPACING]>,
    map: Vec<i16>,
    map_size: (usize, usize),
}

impl ThreadState {
    fn dispatch(mut self, map: Arc<Vec<Pixel>>, map_size: (usize, usize), incoming: Receiver<ThreadMsg>, outgoing: Sender<(usize, u64, (usize, usize))>) {
        thread::spawn(move|| {
            self.calc_map(map, map_size);
            for msg in incoming.iter() {
                match msg {
                    CalcDiff(i) => {
                        let start = Instant::now();
                        let mut best = u64::max_value();
                        let mut best_pos = (0, 0);
                        for y in ((SAMPLE_SIZE.1+self.offset)..(self.map_size.1 - SAMPLE_SIZE.1)).step_by(LINE_SPACING) {
                            for x in SAMPLE_SIZE.0..(self.map_size.0 - SAMPLE_SIZE.0) {
                                let r = self.calc_diff(i, x, y);
                                if r < best {
                                    best = r;
                                    best_pos = (x, y);
                                }
                            }
                        }
                        println!("stitching: {}ms", (Instant::now() - start).as_millis());
                        outgoing.send((i, best, best_pos)).unwrap();
                    },
                    MapUpdate{ map, map_size, samples } => {
                        let start = Instant::now();
                        for i in samples {
                            self.samples.remove(i);
                        }
                        self.calc_map(map, map_size);
                        println!("updating map: {}ms", (Instant::now() - start).as_millis());
                    },
                }
            }
        });
    }

    fn calc_diff(&self, sample_num: usize, x: usize, y: usize) -> u64 {
        for iy in [0, SAMPLE_SIZE.1 - 1] {
            for ix in [0, SAMPLE_SIZE.0 - 1] {
                let x = x + ix - SAMPLE_SIZE.0 / 2;
                let y = y + iy - SAMPLE_SIZE.1 / 2;
                if self.map[x + y * self.map_size.0] == i16::max_value() { //early out for blank sections
                    return u64::max_value();
                }
            }
        }
        
        let mut sum = 0u64;
        for iy in (0..SAMPLE_SIZE.1).step_by(LINE_SPACING) {
            for ix in 0..SAMPLE_SIZE.0 {
                let i = ix + iy * SAMPLE_SIZE.0 / LINE_SPACING;
                let x = x + ix - SAMPLE_SIZE.0 / 2;
                let y = y + iy - SAMPLE_SIZE.1 / 2;
                let sample = unsafe { self.samples[sample_num].get_unchecked(i) };
                let map = unsafe { self.map.get_unchecked(x + y * self.map_size.0) };
                sum += (sample - map).abs() as u64;
            }
        }
        sum
    }

    fn calc_map(&mut self, map: Arc<Vec<Pixel>>, size: (usize, usize)) {
        self.map_size = size;
        self.map = vec![i16::max_value(); map.len()];
        for x in 0..size.0 {
            for y in 0..size.1 {
                let [r,g,b,a] = map[x + y * size.0];
                if a != 0 {
                    self.map[x + y * size.0] = to_grayscale(r,g,b);
                }
            }
        }
    }
}

fn save_output((w, h): (usize, usize), map: &[Pixel]) {
    let mut img = RgbaImage::new(w as u32, h as u32);

    for (y, row) in map.chunks(w).enumerate() {
        for (x, pixel) in row.iter().enumerate() {
            img.put_pixel(x as u32, y as u32, Rgba(*pixel));
        }
    }

    img.save("output.png").unwrap_or_else(|e| {
        eprintln!("Failed to save the output image: {}", e);
    });
}

fn update_map((mut x, mut y): (usize, usize), map_size: &mut (usize, usize), old: &[Pixel], image: &[Pixel], size: usize) -> Vec<Pixel> {
    let left = x as isize - size as isize / 2;
    let right = x + size as usize / 2;
    let top = y as isize - size as isize / 2;
    let bottom = y + size as usize / 2;
    let new_w = (right.max(map_size.0) as isize - left.min(0)) as usize;
    let new_h = (bottom.max(map_size.1) as isize - top.min(0)) as usize;

    let mut map = vec![[0; 4]; new_w * new_h];

    for ix in 0..map_size.0 {
        for iy in 0..map_size.1 {
            let x = ix + left.min(0).abs() as usize;
            let y = iy + top.min(0).abs() as usize;
            map[x + y * new_w] = old[ix + iy * map_size.0];
        }
    }
    *map_size = (new_w, new_h);

    x += left.min(0).abs() as usize;
    y += top.min(0).abs() as usize;

    let rad = size as f64 * size as f64 * 0.25;
    
    for ix in 0..size {
        for iy in 0..size {
            if (ix as f64 - size as f64 / 2.0).powi(2) + (iy as f64 - size as f64 / 2.0).powi(2) >= rad {
                continue; //ignore the circular black edges
            }
            let i = (x + ix - size / 2) + (y + iy - size / 2) * map_size.0;
            let new_pixel = image[ix + iy * size];
            let old_pixel = map[i];
            let new_only = (!old_pixel[3]) & 1;
            let both = old_pixel[3] & 1;
            for j in 0..3 {
                map[i][j] = new_only * new_pixel[j];
                map[i][j] += both * (new_pixel[j] / 2 + old_pixel[j] / 2 + ((new_pixel[j] | old_pixel[j]) & 1));
            }
            map[i][3] = new_pixel[3];
        }
    }
    map
}

fn load_image(path: &std::path::PathBuf, images: &mut Vec<(Vec<Pixel>, usize)>, samples: &mut Vec<Sample>) -> Result<(), image::ImageError> {
    let img = image::open(path)?;
    
    let size = BASE_SIZE * {
        let check = img.grayscale();
        
        if check.get_pixel(192, 100).0[0] as i32 > 10 || check.get_pixel(192, 903).0[0] as i32 > 10 {
            1
        } else if check.get_pixel(196, 100).0[0] as i32 > 10 || check.get_pixel(196, 903).0[0] as i32 > 10 {
            2
        } else if check.get_pixel(198, 99).0[0] as i32 > 10 || check.get_pixel(198, 902).0[0] as i32 > 10 {
            3
        } else {
            4
        }
    };
    
    let img = img.resize(size as u32, size as u32, image::imageops::FilterType::Lanczos3).to_rgba8();
    let mut image = vec![[0; 4]; size * size];
    //let rad = size as f64 * size as f64 * 0.25;
    for y in 0..size {
        for x in 0..size {
            /*if (x as f64 - size as f64 / 2.0).powi(2) + (y as f64 - size as f64 / 2.0).powi(2) >= rad {
                continue; //ignore the circular black edges
            }*/ //we're doing this in update_map instead
            image[x + y * size] = img.get_pixel(x as u32, y as u32).0;
        }
    }

    let mut sample = [0; SAMPLE_SIZE.0 * SAMPLE_SIZE.1 / LINE_SPACING];
    for iy in (0..SAMPLE_SIZE.1).step_by(LINE_SPACING) {
        for ix in 0..SAMPLE_SIZE.0 {
            let i = ix + iy * SAMPLE_SIZE.0 / LINE_SPACING;
            let x = (size  - SAMPLE_SIZE.0) / 2 + ix;
            let y = (size  - SAMPLE_SIZE.1) / 2 + iy;
            let [r,g,b,_a] = image[x + y * size];
            sample[i] = to_grayscale(r,g,b);
        }
    }

    samples.push(sample);
    images.push((image, size));

    Ok(())
}

fn to_grayscale(r: u8, g: u8, b: u8) -> i16 {
    r as i16 + g as i16 + b as i16
}
